/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tehas
 */
public class StatusTest {
    
    public StatusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCode method, of class Status.
     */
    @Test
    public void testGetCode() {
        System.out.println("getCode");
        Status instance = new Status();
        instance.setCode(0);
        int expResult = 0;
        int result = instance.getCode();
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of getMessage method, of class Status.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        Status instance = new Status();
        instance.setMessage("Hello");
        String expResult = "Hello";
        String result = instance.getMessage();
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of toString method, of class Status.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Status instance = new Status();
        
        instance.setCode(0);
        instance.setMessage("Test");
        String expResult = "0: Test";
        String result = instance.toString();
        assertEquals(expResult, result);
       
    }
    
}

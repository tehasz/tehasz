/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tehas
 */
public class RuntimeTest {
    
    public RuntimeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActualTemperature method, of class Runtime.
     */
    @Test
    public void testGetActualTemperature() {
        System.out.println("getActualTemperature");
        Runtime instance = new Runtime();
        instance.setActualTemperature(50);
        int expResult = 50;
        int result = instance.getActualTemperature();
        assertEquals(expResult, result);
        
    }

   

    /**
     * Test of getActualHumidity method, of class Runtime.
     */
    @Test
    public void testGetActualHumidity() {
        System.out.println("getActualHumidity");
        Runtime instance = new Runtime();
        instance.setActualHumidity(75);
        int expResult = 75;
        int result = instance.getActualHumidity();
        assertEquals(expResult, result);
        
    }

   

    /**
     * Test of toString method, of class Runtime.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Runtime instance = new Runtime();
        instance.setActualHumidity(50);
        instance.setActualTemperature(75);
        String expResult = "T: 75\t-\tH: 50";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}

package programmablethermostat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ProgrammableThermostat {

    public static void main(String[] args) {
        
        
        System.out.println("Programmable Thermostat v1.0 ©2019");

        URL jsonURL = null;
        try {
            jsonURL = new URL("http://media.capella.edu/BBCourse_Production/IT4774/temperature.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {

            byte[] jsonData = Files.readAllBytes(Paths.get("./temperature.json"));

            ObjectMapper objectMapper = new ObjectMapper();

            // Commented this out until client fixes his JSON output
            // Thermostat T = objectMapper.readValue(jsonURL, Thermostat.class);

            // Getting the JSON from our local file for the time being
            Thermostat T = objectMapper.readValue(jsonData, Thermostat.class);

            System.out.println(T.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        
    }
    
}

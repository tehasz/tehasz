package programmablethermostat;

public class Status {

    private int code;
    private String message;

    public Status() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        if (message.equals("")) {
            return "No Message";
        }

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return Integer.toString(getCode()) + ": " + getMessage();
    }
}

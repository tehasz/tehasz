package programmablethermostat;

public class Runtime {

    private int actualTemperature;
    private int actualHumidity;

    public Runtime() {
    }

    public int getActualTemperature() {
        return actualTemperature;
    }

    public void setActualTemperature(int actualTemperature) {
        this.actualTemperature = actualTemperature;
    }

    public int getActualHumidity() {
        return actualHumidity;
    }

    public void setActualHumidity(int actualHumidity) {
        this.actualHumidity = actualHumidity;
    }


    @Override
    public String toString() {

        return "T: " + getActualTemperature() + "\t-\tH: " + getActualHumidity();

    }


}
